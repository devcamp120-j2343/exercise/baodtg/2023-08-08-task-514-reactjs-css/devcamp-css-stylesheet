import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

import image from './assets/images/48.jpg'

function App() {
  return (
    <div className="dc-container">
      <div className="dc-image-container">
        <img src={image} alt='image user' className='dc-image'></img>
      </div>
      <div className='dc-quote'>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      </div>
      <div className='dc-info'>
        <b className='user-name'>Tammy Stevent </b> &nbsp; * &nbsp;Front End Developer
      </div>
    </div>
  );
}

export default App;
